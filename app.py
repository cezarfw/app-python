#!/usr/bin/python3

import json
from botocore.exceptions import ClientError
import boto3
import sys
import os
from datetime import datetime
from http.server import BaseHTTPRequestHandler, HTTPServer


# HTTPRequestHandler class
class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):

  # GET
  def do_GET(self):
        # Enviando codigo de status da resposta
        self.send_response(200)

        # Enviando headers
        self.send_header('Content-type','text/html')
        self.end_headers()

        # Enviando mensagem de volta ao cliente
        message = "Hello Conta Azul!"
        
        # Dados no formato utf
        self.wfile.write(bytes(message, "utf8"))

        # Validacao dos argumentos passados e chamando a funcao gera_log
        bucket = sys.argv[1]
        if len(sys.argv)==3 : 
            nome_arquivo = sys.argv[2]
            gera_log(bucket, nome_arquivo)
        
        # Chamando a funcao gera_log
        gera_log(bucket, nome_arquivo=None)

        return


def run():
  print('Iniciando servidor...')

  # Configuracoes do servidor
  server_address = ('0.0.0.0', 8081)
  httpd = HTTPServer(server_address, testHTTPServer_RequestHandler)
  print('Servidor em execucao, acesse a porta 8081...')
  httpd.serve_forever()


# Funcao para o processamento e envio ao bucket S3 conforme solicitados no teste.
def gera_log(bucket_name, nome_arquivo=None):

    hostname = os.uname()[1]
    data_e_hora_atuais = datetime.now()
    data_atual = data_e_hora_atuais.strftime('%d-%m-%Y')
    hora_atual = data_e_hora_atuais.strftime('%H:%M')

    try:
        if nome_arquivo is None:
            s3 = boto3.resource('s3')
            object = s3.Object(bucket_name, 'cenario-a.txt')
            object.put(Body=hostname + " " + data_atual + " " + hora_atual)

        else:
            s3 = boto3.resource('s3')
            object = s3.Object(bucket_name, nome_arquivo)
            object.put(Body=hostname + " " + data_atual + " " + hora_atual)

    except ClientError as e:
        logging.error(e)
        return False
    return

run()